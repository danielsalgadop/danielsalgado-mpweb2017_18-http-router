<?php

namespace UnitTests;

use Router\Router;
use PHPUnit_Framework_TestCase;

final class RouterWithoutParamsTest extends PHPUnit_Framework_TestCase
{
    public function routesWithoutParams()
    {
        return[
                [
                    [
                        'route/level1/level2/level3',
                        'route1',
                        'route2',
                        'route3/level1',
                        'route111',
                        'route11',
                        'route/level1/level2',
                    ]
                ]
        ];
    }


    /**
    * @test
    */
    public function uriAndRoutesEqual()
    {
        $router = new Router(['bla','route3/level1']);
        $response = $router->match('route3/level1');
        self::assertTrue(1 == $response);
    }
}
