<?php

namespace UnitTests;

use Router\Router;
use PHPUnit_Framework_TestCase;

final class RouterTest extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function sameStringForRouteAndForUriReturns0()
    {
        $same_string = 'size1';
        $router = new Router([$same_string]);
        self::assertTrue(0 === $router->match($same_string));
    }

    /** @test */
    public function differentStringReturnBooleanFalse()
    {
        $router = new Router(['size1']);
        $result_match = $router->match('foo');
        self::assertTrue(gettype($result_match) === 'boolean');
        self::assertTrue($result_match === false);
    }

    /** @test */
    public function routesWitoutParamsShouldMatchIdenticallUris()
    {
        $arr_routes = [
                             'level1',
                             'level1/level2',
                             'level1/level2/level3'
                             ];
        $router = new Router($arr_routes);
        foreach ($arr_routes as $expected =>$uri) {
            self::assertTrue($router->match($uri) == $expected);
        }
    }


    /**
    * @test
    * @dataProvider complexDataSetBecauseHasMultipleParamsProvider
    */
    public function complexRoutesWithMultipleParamsReturnExpectedId($complex_dataset)
    {
        $router = new Router($complex_dataset['routes_for_router']);

        foreach ($complex_dataset['test_uri'] as $expected_id => $arr_uris) {

            foreach ($arr_uris as $uri) {
                self::assertTrue($expected_id === $router->match($uri));
            }
        }
    }

    /**
    * @test
    *
    */
    public function routesWith1ParamMatchCorrectRouteId()
    {
        $router = new Router(['level1/{param1}']);
        self::assertTrue(0 === $router->match('level1/X'));
    }

    /** @test */
    public function simple2Level1Param()
    {
        $arr = ['params/{1}'];
        $router = new Router($arr);
        $uri = 'params/1';
        self::assertTrue(0 ===  $router->match($uri));
    }

    // dataProviders
    public function complexDataSetBecauseHasMultipleParamsProvider()
    {
        return [
             [
                 [
                    'routes_for_router' =>
                    [
                        '/level1/{param1}',
                        '/level1/level2/{param1}',
                        '/level1/level2/{param1}/level3/{param2}/level4',
                    ],
                    'test_uri' =>
                    [
                        0 =>
                            [
                                '/level1/1',
                                '/level1/2',
                                '/level1/3',
                            ],
                        1 =>
                            [
                                '/level1/level2/1',
                                '/level1/level2/2',
                                '/level1/level2/3',
                            ]
                        ],
                        2 =>
                            [
                                '/level1/level2/1/level3/{param2}/level4',
                                '/level1/level2/2/level3/{param2}/level4',
                                '/level1/level2/3/level3/{param2}/level4',
                            ]
                    ]
                ],
            ];
    }


}
