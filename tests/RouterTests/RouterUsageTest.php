<?php

namespace UnitTests;

use Router\Router;
use PHPUnit_Framework_TestCase;

final class RouterUsageTest extends PHPUnit_Framework_TestCase
{
    /**
    * @test
    * @expectedException \Router\Exception\EmptyRoutesException
    *  */
    public function emtpyRouteThrowsException()
    {
        new Router([]);
    }
    /**
    * @test
    * @expectedException \Router\Exception\NotValidSegmentException
    *  */
    public function emtpyUriThrowsException()
    {
        $router = new Router(['size1']);
        $router->match('');
    }
}
