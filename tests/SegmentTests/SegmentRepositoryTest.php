<?php

namespace UnitTests;

use Router\SegmentRepository;
use PHPUnit_Framework_TestCase;

final class SegmentRepositoryTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function comparingSegmentsReturnsBoolean()
    {
        $string_size1_segment_repo = 'size1';
        $size1_segment_repo = new SegmentRepository($string_size1_segment_repo);
        $must_be_an_array = $size1_segment_repo->comparingSegments($size1_segment_repo);
        self::assertTrue('boolean' === gettype($must_be_an_array), 'must return a boolean');
    }

    /**
     * @test
     */
    public function comparingDiferentSizeSegmentsReturnFalse()
    {
        $string_size1_segment_repo = 'size1';
        $string_size2_segment_repo = 'size1/size2';
        $size1_segment_repo = new SegmentRepository($string_size1_segment_repo);
        $size2_segment_repo = new SegmentRepository($string_size2_segment_repo);
        self::assertFalse($size1_segment_repo->comparingSegments($size2_segment_repo));
        self::assertFalse($size2_segment_repo->comparingSegments($size1_segment_repo));
    }



    /**
     * @test
     * TODO dataProvider
     */
    public function comparingSameSizeSegmentsReturnTrue()
    {
        $arr_routes = ['size1','size1/size2', 'size1/size2/size3'];
        foreach ($arr_routes as $string_route) {
            $segment_repo1 = new SegmentRepository($string_route);
            $segment_repo2 = new SegmentRepository($string_route);
            self::assertTrue($segment_repo1->comparingSegments($segment_repo2), 'comapring this repo '.$segment_repo1. ' with this repo '.$segment_repo2);
            self::assertTrue($segment_repo2->comparingSegments($segment_repo1));
        }
    }

    /**
     * @test
     */
    public function comparingDiferentLevel1SegmentsReturnFalse()
    {
        $string_segment_repo = 'level1';
        $string_segment_repo2 = 'diferent';
        $segment = new SegmentRepository($string_segment_repo);
        $segment2 = new SegmentRepository($string_segment_repo2);
        self::assertFalse($segment->comparingSegments($segment2));
    }

    /**
    * @test
    * @dataProvider withSlashes
    *  */
    public function firstAndLastSlashAreRemoved($expected, $route_with_slashes)
    {
        $segment_repository = new SegmentRepository($route_with_slashes);
        self::assertTrue($expected  == $segment_repository);
    }
    public function withSlashes()
    {
        return [
                ['level1', 'level1'],
                ['level1', '/level1/'],
                ['level1', 'level1/'],
                ['level1', '/level1'],
                ['level1/level2', 'level1/level2'],
                ['level1/level2', '/level1/level2/'],
                ['level1/level2', 'level1/level2/'],
        ];
    }


    /** @test */
    public function buildSegmentRepository1Level()
    {
        $route = '/level1/';
        $segment_repository = new SegmentRepository($route);
        self::assertTrue($segment_repository == 'level1');
        $collection_segment_repos = $segment_repository->getCollection();
        self::assertTrue(count($collection_segment_repos) == 1);
        self::assertTrue($collection_segment_repos[0] == 'level1');
    }

    /** @test */
    public function buildSegmentRepository2Levels()
    {
        $route = '/level1/level2/';
        $segment_repository = new SegmentRepository($route);
        self::assertTrue($segment_repository == 'level1/level2');

        $collection_segment_repos = $segment_repository->getCollection();
        self::assertTrue(count($collection_segment_repos) == 2);
        self::assertTrue($collection_segment_repos[0] == 'level1');
        self::assertTrue($collection_segment_repos[1] == 'level2');
    }
}
