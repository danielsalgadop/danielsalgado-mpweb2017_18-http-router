<?php

namespace UnitTests;

use Router\Segment;
use Router\Exception\NotValidSegmentException;
use PHPUnit_Framework_TestCase;

final class SegmentTest extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function validSegmentStringsCreateSegmentInstances()
    {
        $arr_valid_strings = [
            'alpha',
            'ALPHA',
            '12345',
            'a_b_c_',
            '1_2_3_4',
            '{x}',
            '{param}',
        ];
        foreach ($arr_valid_strings as $valid_string) {
            self::assertInstanceOf(Segment::class, new Segment($valid_string));
        }
    }


    /**
    * @test
    * @expectedException \Router\Exception\NotValidSegmentException
    */
    public function slashThrowsException()
    {
        new Segment('/');
    }

    /**
    * @test
    * @expectedException \Router\Exception\NotValidSegmentException
    */
    public function emtpyParamsThrowsException()
    {
        new Segment('{}');
    }

    /**
    * @test
    * @expectedException \Router\Exception\NotValidSegmentException
    */
    public function dollarSignThrowsException()
    {
        new Segment('$');
    }

    /** @test */
    public function uriAndRoutesEqual()
    {
        $routerSegment = new Segment('segment_value');
        $uriSegment = new Segment('segment_value');

        self::assertTrue($routerSegment->compareSegment($uriSegment), "EqualSegments must be equal ".$routerSegment->getValue(). " ".$uriSegment->getValue());
    }


    /** @test */
    public function uriAndRoutesNotEqual()
    {
        $routerSegment = new Segment('segment_value');
        $uriSegment = new Segment('NOT_Equalsegment_value');

        self::assertFalse($routerSegment->compareSegment($uriSegment), "EqualSegments must be equal ".$routerSegment->getValue());
    }

    /** @test */
    public function valueContainersMatchAnything()
    {
        $routerSegment = new Segment('{x}');
        $uriSegment = new Segment('anything');

        self::assertTrue($routerSegment->compareSegment($uriSegment), "must match");
    }
}
