Crea un Router HTTP y testéalo. 

Crea un Router HTTP que a partir de un conjunto de rutas comparadas con una URI, devuelva el id de la primera ruta que coincide con la URI. Las rutas pueden tener parámetros variables, por ejemplo, la comparar la URI /post/1 con la ruta /post/{id} devuelve cierto. 

Para crear rutas con parámetros variables puedes ayudarte de las expresiones regulares. Ayúdate de la pregunta y respuesta planteadas en el foro de preguntas: https://estudy.salle.url.edu/mod/forum/view.php?id=328190  Cómo extraer parámetros de una ruta. Recuerda que NO es obligatorio extraer los parámetros. 

Haz tests de las diferentes funcionalidades. Haz el mayor número de tests unitarios para abarcar el mayor número de posibilidades. 

A entregar:

URL del repositorio del Router. 

El nombre del fichero txt a entregar debe ser el mismo que el nombre del repositorio.
