<?php

namespace Router;

use Router\Exception\NotValidSegmentException;

/**
* DTO
*/
class Segment
{
    private $value;
    private $is_param_container = false;
    public function __construct(string $string)
    {
        if (!$this->validStringSegment($string)) {
            throw new NotValidSegmentException('Not valid String for Segment', 0);
        }
        $this->value = $string;
        $this->isParamContainer();
    }
    private function isParamContainer()
    {
        if (preg_match('/^\{.+\}$/', $this->value)) {
            $this->is_param_container = true;
        }
    }
    public function compareSegment(Segment $uri_segment)
    {
        if ($this->is_param_container) {
            return true;
        }
        if ($this->value === $uri_segment->getValue()) {
            return true;
        }
        return false;
    }
    public function getValue()
    {
        return $this->value;
    }
    private function validStringSegment(string $string)
    {
        if (preg_match('/^[a-zA-Z0-9_]+$/', $string) || preg_match('/^{[a-zA-Z0-9_]+}$/', $string)) {
            return true;
        }
        return false;
    }
    public function __toString()
    {
        return $this->value;
    }
}
