<?php

namespace Router;

use Router\Segment;

class SegmentRepository
{
    private $collection = [];

    public function __construct(string $route)
    {
        $route = $this->cleanFirstAndLastSlashes($route);
        $arr = explode('/', $route);
        foreach ($arr as $string_segment) {
            $this->collection[] = new Segment($string_segment);
        }
    }

    private function cleanFirstAndLastSlashes($string) :string
    {
        $string = preg_replace('!^/!', '', $string);
        return preg_replace('!/$!', '', $string);
    }


    public function comparingSegments(SegmentRepository $uri): bool
    {
        $result = false;
        if (! $this->haveSameLength($uri)) {
            return false;
        }

        for ($i=0;$i<count($this->collection);$i++) {
            $route_segment = $this->getSegmentByID($i);
            $uri_segment = $uri->getSegmentByID($i);

            if ($route_segment->compareSegment($uri_segment)) {
                $result = true;
            } else {
                return false;
            }
        }
        return $result;
    }

    private function getSegmentByID(int $id)
    {
        return $this->collection[$id];
    }

    private function haveSameLength(SegmentRepository $uri)
    {
        return(count($this->collection) == count($uri->getCollection()));
    }

    /**
     * @return mixed
     */
    public function getCollection()
    {
        return $this->collection;
    }
    public function __toString()
    {
        return implode('/', $this->collection);
    }
}
