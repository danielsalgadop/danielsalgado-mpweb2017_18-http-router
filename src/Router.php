<?php

namespace Router;

use Router\SegmentRepository;
use Router\Exception\EmptyRoutesException;

/**
*
*/
class Router
{
    private $arr_segment_repository;
    public function __construct(array $arr_routes)
    {
        if ($arr_routes === []) {
            throw new EmptyRoutesException;
        }
        foreach ($arr_routes as $route) {
            $this->arr_segment_repository[] = new SegmentRepository($route);
        }
    }
    public function match(string $uri)
    {
        for ($i = 0; $i<count($this->arr_segment_repository);$i++) {
            if ($this->arr_segment_repository[$i]->comparingSegments(new SegmentRepository($uri))) {
                return $i;
            }
        }
        return false;
    }
}
